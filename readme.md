# Tweet Acrostics
Solution by John Griffing  
The solution is written in ES2015 JavaScript. I primarily used standard ES2015 and node packages, I also used eslint, and jest--a unit test framework built on top of jasmine. I considered using Rx.js, but decided that it was unnecessary and that promises and event emitters would suffice.

The code is hosted in a bitbucket [repository](https://bitbucket.org/Griffingj/tweet-acrostics).
This application requires node.js, you may download the latest stable version [here](https://nodejs.org/en/).  

Run ```npm install``` to install dependencies.  
Run ```npm start``` to parse the files and output the results to console.  
Run ```npm test``` if you are interested in running the tests.  

## Primary Code Components
#### tweet-acrostics.js
Adapt the files to streams, setup a parser which deals in streams, trigger the parsing and respond to any matches by printing them to console as they arise.

```javascript
import fs       from 'fs';
import readline from 'readline';
import Parser   from './parser';

const lookupPath = 'resource/words-lowercase.txt';
const corpusPath = 'resource/tweets.txt';

// Adapt the files to stream interfaces
const lookupReadStream = readline.createInterface({
  input: fs.createReadStream(lookupPath)
});

// Pause the stream up front, because it will enter flow mode otherwise.
lookupReadStream.pause();

const corpusReadStream = readline.createInterface({
  input: fs.createReadStream(corpusPath)
});

// Pause the stream up front, because it will enter flow mode otherwise.
corpusReadStream.pause();

// Initialize the parser. The lookup file will be read here. Presume it's okay
// to put the entire lookup file contents in memory so that the lookup is fast
const parser = new Parser(lookupReadStream);

// Configure listening for results
parser.on('result', result => {
  console.log(`${result[0]} - ${result[1]}`); // eslint-disable-line no-console
});

// Parse the tweets
parser.parse(corpusReadStream);
```

#### parser.js
The lookup table is loaded on object creation in the constructor as it characterizes the identity of the parser. When parse is called await completion of lookup table reading, if necessary, then read from the corpus stream and emit any matches. The point of dealing in streams was to convey flexibility, so that parser could operate on anything convertible into a stream.

```javascript
import parseTweet       from './parse-tweet';
import { EventEmitter } from 'events';

// Create the lookup HashSet
function lookupFromStream(lookupReadStream) {
  return new Promise(resolve => {
    const lookup = new Set();

    lookupReadStream.on('line', line => lookup.add(line));
    lookupReadStream.on('close', () => {
      resolve(lookup);
    });
    lookupReadStream.resume();
  });
}

export default class Parser extends EventEmitter {
  constructor(lookupReadStream) {
    super();
    // Load the lookup table from the stream when the object is created
    // this parser may then be used to look for acrostics against multiple
    // different sources thereafter
    this.lookup = lookupFromStream(lookupReadStream);
  }

  parse(corpusReadStream) {
    return this.lookup.then(lookup => {
      corpusReadStream.on('line', line => {
        const candidate = parseTweet(line);

        if (candidate.length > 3 && lookup.has(candidate)) {
          this.emit('result', [candidate, line]);
        }
      });
      corpusReadStream.resume();
    });
  }
}
```

#### parse-tweet.js
Distill a tweet into a shape formatted to be tested against the lookup table.

```javascript
export default function parseTweet(tweet) {
  return tweet.split(/\s+/).map(token => token[0] && token[0].toLowerCase()).join('');
}
```

#### Acrostic Results
```
beid - Batakda eşin iyi değilse
bark - Bir anda rahatladığın kitaplar...
wiss - Wow im so sore
immi - I miss my iPad
massa - My attention span short af
mias - me: it's a surprise
diet - Dios incomparable eres tú
most - maybe others should too
iconic - I C O N I C
toad - Theory of a Deadman&gt;
thio - Turning hope into obsession
antre - agr noes ta rindo ehehuhue
coop - Cadê o onibus porra?!
base - boys are so embarrassing
tete - Takip edene tatil ederim
waif - Why am i freezing😭
amor - a m o r.
amor - a m o r
rimy - Remember I made you
omit - Oh my, Isco's touch.
sage - Satarsan alonsoyu görürsün ebeninkini
dash - Dope ass song http://t.co/CUXgW026zf
asok - Allah, sizi ona kavuştursun..
stag - Star TV'de altyazı geçiyor;
odel - One Direction en Latinoamerica?
hand - have a nice day!
omit - o meu irmao tem;
barn - Bored asf right now
witess - Why is the everything so stressful
wall - Walsall are loving life
hand - have a nice day!
hand - have a nice day!
spass - Some people are so selfish
geum - grind em up maya😭😭💀💀💀
tete - TAKİP EDENİ TATİL EDERİM
sasa - Se agacha se agacha
hech - High End Cleaning  https://t.co/66nO3xDc5V
alca - A la cadenita ah
hosta - how others say they are
tete - TAKİP EDENİ TATİL EDERİM
follow - F O L L O W
abas - as brothers and sisters.
bias - Baby I'm a sociopath
affy - A follow from you
shou - Soğuk havalarda onları unutmayalım!.
nodal - No one deserves a liar
tite - This is the end.
sang - Such a nice game
scyt - siain cewek yang t…
lith - Lazarus in the house.
same - s a m e
alto - And listening to oxy…
gutt - Getting used to this😂
tuba - Ta uma bagunça aqui
deal - Dovrei essere a letto
egbo - everything's gonna be ok
sate - siempre aquí te espero
plan - Pershing lame af na
cask - Cattle assignment strategies.: knm
sert - Stellinare e ritwittare tutti
esth - Ele sabe tudo. http://t.co/ERJrZFUND6
yaba - yaşasın aybukenin babası asdsadsdads
wash - we are super... http://t.co/sWrRbOBDXY
thad - These hips are dangerous
imshi - I mean so have I
soar - SO OBRIGADA A RIR
lina - Love is not arrogant
pact - Predicting a cancellation tomorrow
soma - Santi ortiz me ama
alas - ALSO, LIKE AND SUBSCRIBE!
lath - lmao allllllll t http://t.co/13nVXrPHUU
flob - Face lift or Botox?
taft - The Absolutely Fabulous Thunderbirds
match - My attitude towards college: https://t.co/8VaStW8SSV
duff - Designated ugly fat friend
lina - Love is not arrogant
sudd - Sabah uyandım duygulu duygulu
prig - per rimettersi in gioco!
hask - Herkesin acısı, sevgisi kadar.
skiv - Sadece konuşup,dertleşmeye ihtiyacım var
kola - kiss of life aHAHAHHA
gait - Got a interview tomorrow😈
yeth - Ya en Tijuana http://t.co/hstzkJajOI
mari - minute and read it?
adai - Another day, another injury
sugh - sadece uyurken güzel hayat
special - S P E C I A L
will - wish it lasted lo…
ganja - Got a new job ayyyyyy
momo - McCandless: Okay, Mike. Over.
mari - minute and read it?
seba - Só esse bm amanhã
dama - Dodo avec mon am'!
erma - estpy re mamado ajjsjajsjajsjs
hask - Herkesin acısı, sevgisi kadar
taum - TE AMOO UN MON…
sish - Salut i Swing. http://t.co/q8j8JpAUkC
clot - Con los otros tmbb
wait - W A I T
tiff - Today, I feel fugly
decal - don't even come around lol
lina - Love is not arrogant
alem - a llegado el momento!
steg - Se terminó el Gpe-Reyes
temp - Todo es muy paja
depa - Descanse em paz avô.
```