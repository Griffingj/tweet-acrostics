export default function parseTweet(tweet) {
  return tweet.split(/\s+/).map(token => token[0] && token[0].toLowerCase()).join('');
}
