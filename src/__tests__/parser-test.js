import Parser           from '../parser';
import { EventEmitter } from 'events';

describe('parser', () => {
  it('reads a lookup set from a stream', (done) => {
    const lookupStream = new EventEmitter();
    lookupStream.resume = () => {};

    const parser = new Parser(lookupStream);
    const basis = ['aardvak', 'beryllium', 'cantaloupe'];

    basis.forEach(token => lookupStream.emit('line', token));
    lookupStream.emit('close');

    parser.lookup.then(lookup => {
      expect(lookup).toEqual(new Set(basis));
      done();
    });
  });

  it('emits a result when a match is found', (done) => {
    const lookupStream = new EventEmitter();
    const reduced = 'demo';
    const tweet = 'destroying edible maniacal organisms!!!';

    lookupStream.resume = () => {};

    const parser = new Parser(lookupStream);

    lookupStream.emit('line', reduced);
    lookupStream.emit('close');

    parser.on('result', result => {
      expect(result[0]).toBe(reduced);
      expect(result[1]).toBe(tweet);
      done();
    });

    const corpusStream = new EventEmitter();
    corpusStream.resume = () => {};

    parser.parse(corpusStream).then(() => {
      corpusStream.emit('line', tweet);
      corpusStream.emit('close');
    });
  });
});
