import parseTweet from '../parse-tweet';

describe('parseTweet', () => {
  it('composes the 1st letter of each word', () => {
    const tweet = 'destroying edible maniacal organisms!!!';
    const reduced = 'demo';
    expect(parseTweet(tweet)).toBe(reduced);
  });

  it('composes strings with symbols', () => {
    const tweet = 'تشوف 【レビュー】iPhone/ ぜひとも！！！';
    const reduced = 'ت【ぜ';
    expect(parseTweet(tweet)).toBe(reduced);
  });

  it('converts latin letters to their lowercase form', () => {
    const tweet = 'Destroying edible Maniacal organisms';
    const reduced = 'demo';
    expect(parseTweet(tweet)).toBe(reduced);
  });
});
