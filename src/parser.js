import parseTweet       from './parse-tweet';
import { EventEmitter } from 'events';

// Create the lookup HashSet
function lookupFromStream(lookupReadStream) {
  return new Promise(resolve => {
    const lookup = new Set();

    lookupReadStream.on('line', line => lookup.add(line));
    lookupReadStream.on('close', () => {
      resolve(lookup);
    });
    lookupReadStream.resume();
  });
}

export default class Parser extends EventEmitter {
  constructor(lookupReadStream) {
    super();
    // Load the lookup table from the stream when the object is created
    // this parser may then be used to look for acrostics against multiple
    // different sources thereafter
    this.lookup = lookupFromStream(lookupReadStream);
  }

  parse(corpusReadStream) {
    return this.lookup.then(lookup => {
      corpusReadStream.on('line', line => {
        const candidate = parseTweet(line);

        if (candidate.length > 3 && lookup.has(candidate)) {
          this.emit('result', [candidate, line]);
        }
      });
      corpusReadStream.resume();
    });
  }
}
