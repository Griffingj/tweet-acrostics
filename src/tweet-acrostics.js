import fs       from 'fs';
import readline from 'readline';
import Parser   from './parser';

const lookupPath = 'resource/words-lowercase.txt';
const corpusPath = 'resource/tweets.txt';

// Adapt the files to stream interfaces
const lookupReadStream = readline.createInterface({
  input: fs.createReadStream(lookupPath)
});

// Pause the stream up front, because it will enter flow mode otherwise.
lookupReadStream.pause();

const corpusReadStream = readline.createInterface({
  input: fs.createReadStream(corpusPath)
});

// Pause the stream up front, because it will enter flow mode otherwise.
corpusReadStream.pause();

// Initialize the parser. The lookup file will be read here. Presume it's okay
// to put the entire lookup file contents in memory so that the lookup is fast
const parser = new Parser(lookupReadStream);

// Configure listening for results
parser.on('result', result => {
  console.log(`${result[0]} - ${result[1]}`); // eslint-disable-line no-console
});

// Parse the tweets
parser.parse(corpusReadStream);
